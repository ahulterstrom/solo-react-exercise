import { createStyles } from "@mantine/core";

const useStyles = createStyles((theme) => ({
  breadcrumbs: {
    fontSize: 18,
  },
  thead: {
    backgroundColor:
      theme.colorScheme === "dark"
        ? theme.colors.dark[7]
        : theme.colors.gray[1],
  },
  infoBox: {
    backgroundColor:
      theme.colorScheme === "dark"
        ? theme.colors.dark[7]
        : theme.colors.gray[1],
  },
  td: {
    paddingTop: 4,
    paddingBottom: 4,
  },
}));

export default useStyles;
