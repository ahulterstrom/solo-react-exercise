import {
  Breadcrumbs,
  Button,
  Container,
  Divider,
  Group,
  MantineProvider,
  Paper,
  Select,
  Space,
  Stack,
  Table,
  Text,
  Title,
} from "@mantine/core";
import { useState } from "react";
import useStyles from "./App.styles";

const API_URL = "http://localhost:3000";

interface Representative {
  name: string;
  party: string;
  state: string;
  district: string;
  phone: string;
  office: string;
  link: string;
}

const repOrSenOptions = [
  { label: "Representative", value: "representatives" },
  { label: "Senator", value: "senators" },
];

const stateOptions = [
  { label: "Alabama", value: "AL" },
  { label: "Alaska", value: "AK" },
  { label: "Arizona", value: "AZ" },
  { label: "Arkansas", value: "AR" },
  { label: "California", value: "CA" },
  { label: "Colorado", value: "CO" },
  { label: "Connecticut", value: "CT" },
  { label: "Delaware", value: "DE" },
  { label: "Florida", value: "FL" },
  { label: "Georgia", value: "GA" },
  { label: "Hawaii", value: "HI" },
  { label: "Idaho", value: "ID" },
  { label: "Illinois", value: "IL" },
  { label: "Indiana", value: "IN" },
  { label: "Iowa", value: "IA" },
  { label: "Kansas", value: "KS" },
  { label: "Kentucky", value: "KY" },
  { label: "Louisiana", value: "LA" },
  { label: "Maine", value: "ME" },
  { label: "Maryland", value: "MD" },
  { label: "Massachusetts", value: "MA" },
  { label: "Michigan", value: "MI" },
  { label: "Minnesota", value: "MN" },
  { label: "Mississippi", value: "MS" },
  { label: "Missouri", value: "MO" },
  { label: "Montana", value: "MT" },
  { label: "Nebraska", value: "NE" },
  { label: "Nevada", value: "NV" },
  { label: "New Hampshire", value: "NH" },
  { label: "New Jersey", value: "NJ" },
  { label: "New Mexico", value: "NM" },
  { label: "New York", value: "NY" },
  { label: "North Carolina", value: "NC" },
  { label: "North Dakota", value: "ND" },
  { label: "Ohio", value: "OH" },
  { label: "Oklahoma", value: "OK" },
  { label: "Oregon", value: "OR" },
  { label: "Pennsylvania", value: "PA" },
  { label: "Rhode Island", value: "RI" },
  { label: "South Carolina", value: "SC" },
  { label: "South Dakota", value: "SD" },
  { label: "Tennessee", value: "TN" },
  { label: "Texas", value: "TX" },
  { label: "Utah", value: "UT" },
  { label: "Vermont", value: "VT" },
  { label: "Virginia", value: "VA" },
  { label: "Washington", value: "WA" },
  { label: "West Virginia", value: "WV" },
  { label: "Wisconsin", value: "WI" },
  { label: "Wyoming", value: "WY" },
];

function App() {
  const [repOrSen, setrepOrSen] = useState<string | null>(null);
  const [state, setState] = useState<string | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [repData, setRepData] = useState<Representative[]>([]);
  const [selectedRep, setSelectedRep] = useState<Representative | null>(null);

  const { classes } = useStyles();

  const clearData = () => {
    setRepData([]);
    setSelectedRep(null);
  };

  const handleSelectRepOrSen = (value: string) => {
    if (value !== repOrSen) {
      setrepOrSen(value);
      clearData();
    }
  };

  const handleSelectState = (value: string) => {
    if (value !== state) {
      setState(value);
      clearData();
    }
  };

  const handleSubmit = () => {
    setLoading(true);
    fetch(`${API_URL}/${repOrSen}/${state}`)
      .then((res) => res.json())
      .then((data) => {
        if (data.success) {
          setRepData(data.results);
        }
        setLoading(false);
      });
  };

  return (
    <MantineProvider withGlobalStyles withNormalizeCSS>
      <Container>
        <Space h="xl" />
        <Stack>
          <Title color="blue">Who's My Representative?</Title>
          <Space h="lg" />
          <Divider />
          <Group align="flex-end">
            <Select
              value={repOrSen}
              onChange={handleSelectRepOrSen}
              label="Representative or Senator"
              placeholder="Select Representative or Senator"
              data={repOrSenOptions}
              w={280}
            />
            <Select
              value={state}
              onChange={handleSelectState}
              label="State"
              placeholder="Select state"
              data={stateOptions}
            />
            <Button
              loading={loading}
              disabled={!repOrSen || !state}
              onClick={handleSubmit}
            >
              Find my Representative
            </Button>
          </Group>
          <Group align="flex-start" spacing="xl">
            <Stack sx={{ flex: 1 }}>
              <Breadcrumbs
                h={31}
                classNames={{ separator: classes.breadcrumbs }}
              >
                <Title order={3} weight="normal">
                  List
                </Title>
                <Title order={3} weight="normal" color="blue">
                  Representatives
                </Title>
              </Breadcrumbs>
              <Table highlightOnHover>
                <thead className={classes.thead}>
                  <tr>
                    <th>
                      <Text py="xs" size="md" weight="normal">
                        Name
                      </Text>
                    </th>
                    <th>
                      <Text py="xs" size="md" weight="normal">
                        Party
                      </Text>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {repData.map((representative) => (
                    <tr
                      key={representative.name}
                      onClick={() => setSelectedRep(representative)}
                    >
                      <td>
                        <Text py="xs">{representative.name}</Text>
                      </td>
                      <td>
                        <Text py="xs">{representative.party}</Text>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Stack>
            <Stack sx={{ flex: 1 }} spacing="lg">
              <Title order={3} weight="normal" h={31}>
                Info
              </Title>
              <Paper className={classes.infoBox} p="sm">
                <Text c={selectedRep ? "inherit" : "dimmed"}>
                  {selectedRep?.name.substring(
                    0,
                    selectedRep.name.indexOf(" ")
                  ) || "First Name"}
                </Text>
              </Paper>
              <Paper className={classes.infoBox} p="sm">
                <Text c={selectedRep ? "inherit" : "dimmed"}>
                  {selectedRep?.name.substring(
                    selectedRep.name.indexOf(" ") + 1
                  ) || "Last Name"}
                </Text>
              </Paper>
              <Paper className={classes.infoBox} p="sm">
                <Text c={selectedRep ? "inherit" : "dimmed"}>
                  {selectedRep?.district || "District"}
                </Text>
              </Paper>
              <Paper className={classes.infoBox} p="sm">
                <Text c={selectedRep ? "inherit" : "dimmed"}>
                  {selectedRep?.phone || "Phone"}
                </Text>
              </Paper>
              <Paper className={classes.infoBox} p="sm">
                <Text c={selectedRep ? "inherit" : "dimmed"}>
                  {selectedRep?.office || "Office"}
                </Text>
              </Paper>
            </Stack>
          </Group>
        </Stack>
      </Container>
    </MantineProvider>
  );
}

export default App;
